require('./src/styles/app.scss')

const utils = require('./src/js/utils')
const core = require('./src/js/core')
const exedoc = require('./src/js/exedoc')
const tgr = require('./src/js/tgr')
const pet = require('./src/js/pet')
const ga = require('./src/js/ga')
const grilla = require('./src/js/grilla-de')

const $ = require('jquery')

$(document).ready(function () {
  document.title = 'SUBTRANS - ' + utils.obtenerTituloFormulario()
  if (!utils.etapaActual().editable) {
    $('.campo.control-group input.form-control, .campo.control-group .form-check-input').attr('disabled', 'disabled')
  }
})

module.exports = {
  utils,
  core,
  exedoc,
  tgr,
  pet,
  ga,
  grilla
}