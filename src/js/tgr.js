'use strict'

const core = require('./core')
const utils = require('./utils')

/**
 * Datos requeridos desde formulario para construir la información del pago
 * @typedef {Object} DatosPagoTGRTypedef
 * @property {string} rut - RUT asociado al solicitante 
 * @property {string} razonSocial  - nombre o razon social asociada al rut
 * @property {number} idTramite - id trámite actual
 * @property {string} tramite  - nombre del proceso
 * @property {number} monto  - monto a pagar
 * @property {string} descripcion - contenido adicional
 */

/**
 * @param {DatosPagoTGRTypedef} datosPago 
 */
function crearObjetoPago(datosPago) {
  const fechaVencimiento = new Date(Date.now() + (1 * 86400000))
  const r = datosPago.rut.split('-')
  var objPago = {
    "rut": r[0],
    "dv": r[1],
    "descripcion": (datosPago.descripcion || `Trámite ${datosPago.idTramite}`).substr(0, 30),
    "razonSocial": datosPago.razonSocial.substr(0, 200),
    "vencimiento": utils.toStrDate(fechaVencimiento),
    "monto": datosPago.monto,
    "nombreTramite": datosPago.tramite.substr(0, 100),
    "urlCallbackSimple": `${window.location.protocol}//${window.location.host}/backend/api/notificar/${datosPago.idTramite}`
  }
  return objPago
}


/**
  * @param {string} nombreCampo - nombre del campo conteniendo el valor
  * @param {DatosPagoTGRTypedef} datosPago - datos del pago a efectuar
  */
function definirCampoJsonPago(nombreCampo, datosPago) {
  const $t = core.usarInputComoHidden(nombreCampo)
  const objPago = crearObjetoPago(datosPago)
  $t.val(utils.deshidratar(objPago))
}

module.exports = {
  definirCampoJsonPago
}