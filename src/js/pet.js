'use strict'

const core = require('./core')
const utils = require('./utils')

/**
 * @typedef {Object} DatosPETTypedef
 * @property {string} idTramite - id trámite SIMPLE
 * @property {string} idProceso - id proceso en PET
 * @property {string} region - código de la región del servicio
 * @property {string} rutResponsable
 * @property {string} rutSolicitante
 * @property {string} runMandatario
 * @property {string} nombreProceso - glosa del trámite en SIMPLE
 * @property {string} formaActua 
 * @property {string} estadoSolicitud 
 */

/**
 * definir el campo asociado al valor de registro para PET
 * @param {string} nombreCampo 
 * @param {DatosPETTypedef} datos 
 */
function definirCampoJsonPET(nombreCampo, datos) {
  const $t = core.usarInputComoHidden(nombreCampo)
  const d = Object.assign({ "estado_de_la_solicitud": datos.estadoSolicitud }, datos)
  delete d.estadoSolicitud
  $t.val(utils.deshidratar(d))
}

module.exports = {
  definirCampoJsonPET
}