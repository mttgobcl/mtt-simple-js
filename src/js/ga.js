'use strict'

const utils = require('./utils')

/**
 * @typedef {Object} DatosTramiteGATypedef
 * @property {string} paginaTitulo - titulo personalizado
 */

/**
 * @param {string} codigoGA - código de la propiedad
 * @param {DatosTramiteGATypedef} datosGA - datos anexos para complementar tracking
 * @param {string} globalName - nombre de la function google analitics retornada ('gtag')
 */
function gaInit(codigoGA, datosGA, globalName = 'gtag') {
  const _d = datosGA || {}
  const gtag = function () { dataLayer.push(arguments); }
  window.dataLayer = window.dataLayer || []

  utils.cargarRecurso(`https://www.googletagmanager.com/gtag/js?id=${codigoGA}`, 'js').catch(err => console.log('carga de recurso', err))

  gtag('js', new Date())
  gtag('config', codigoGA, { 'send_page_view': false })
  gtag('config', codigoGA, { 'page_title': _d.paginaTitulo || document.title })


  // gtag('config', codigoGA, { 'page_title': 'titulo personalizado page_title', 'page_location': gaLocation(idTramite) })

  window[globalName] = gtag
  return gtag
}


module.exports = {
  gaInit
}