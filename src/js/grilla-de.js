const utils = require('./utils')

/**
 * @typedef {Object} GrillaDatosExternosModificarOpsTypedef
 * @property {boolean} hidePaginacion - visibilidad de la paginación
 * @property {boolean} hideFiltro - visibilidad del buscador superior  
 * @property {boolean} hideLargo
 * @property {boolean} hideInformacion
 * @property {number} paginaLargo - cantidad de elementos por página
 * @property {number} hideAutomatico - ouculta controles en base a cantidad de datos y paginaLargo
 */

/**
 * toogle modificado para clase css
 * @param {jQuery<HTMLElement>} w - elemento para ocultar
 * @param {boolean} ocultar - si true agrega claseCssOcultar, false eliminar clase claseCssOcultar de elemento indicado 
 * @param {string} claseCssOcultar - si está presente oculta el elemento
 */
function modificaClase(w, ocultar, claseCssOcultar) {
  if (ocultar) {
    $(w).addClass(claseCssOcultar)
  } else {
    $(w).removeClass(claseCssOcultar)
  }
}

/**
 * Modificar el aspecto visual o de funcionamiento de la grilla de datos externos
 * usando estilos css o directamente la configuracion de dataTables si es proporcionada
 * @param {string} nombreCampo 
 * @param {GrillaDatosExternosModificarOpsTypedef} opciones 
 */
function modificarGrilla(nombreCampo, opciones) {
  /** @type {GrillaDatosExternosModificarOpsTypedef} */
  let ops = Object.assign({ paginaLargo: 25 }, opciones)
  const idCampo = utils.getIdCampo(nombreCampo)
  // https://datatables.net/manual/options
  const grilla = $('#grilla-' + idCampo).DataTable()
  if (ops.hideAutomatico) {
    const ocultar = ops.hideAutomatico <= ops.paginaLargo
    Object.assign(ops, {
      hideFiltro: ocultar,
      hideInformacion: ocultar,
      hideLargo: ocultar,
      hidePaginacion: ocultar
    })
  }

  grilla.page.len(ops.paginaLargo)
  grilla.draw()

  // por ahora algunas opciones que no son posibles de modificar por API y necesitan 
  // recrear la tabla, lo que puede causar problemas
  const $w = $(`.campo.control-group[data-id=${idCampo}] .controls.grid-Cls`)
  modificaClase($w, ops.hideFiltro, 'grilla_hide_filter')
  modificaClase($w, ops.hidePaginacion, 'grilla_hide_paginate')
  modificaClase($w, ops.hideInformacion, 'grila_hide_info')
  modificaClase($w, ops.hideLargo, 'grila_hide_length')

  return grilla
}

/**
 * Define datos iniciales para la grilla en caso de que no contenga información o se indique su sobreescritura
 * @param {any[]} datos 
 * @param {string} nombreCampo
 * @param {GrillaDatosExternosModificarOpsTypedef} opciones
 */
function initGrilla(nombreCampo, datos, opciones) {
  const idCampo = utils.getIdCampo(nombreCampo)
  const grilla = grillas_datatable[idCampo]

  const dataTable = modificarGrilla(nombreCampo, opciones)

  if (!grilla.data || !grilla.data.length) {
    if (grilla.is_array) {
      grilla_populate_array(idCampo, datos);
    } else {
      grilla_populate_objects(idCampo, datos);
    }
    store_data_in_hidden(idCampo)
    grilla.table.draw(true);
    grilla.table.columns.adjust();
  }

  return dataTable
}

module.exports = {
  modificarGrilla,
  initGrilla
}