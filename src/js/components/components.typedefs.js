
/**
 * Datos del documento solicitado para ser desplegado como listado
 * @typedef {Object} DocumentoSolicitadoTypedef
 * @property {string} nombre - nombre del documento
 * @property {number|string} codigo - valor o código asociado
 * @property {string} descripcion - descripción alternativa
 * @property {boolean} esObligatorio - obligatorio u opcional
 */

/**
 * @typedef {Object} GrupoDocumentacionTypedef
 * @property {string} idGrupo - identificador de grupo conceptual para el cual se cargan los documentos (ej: patente, rut, solicitante u otro con el que se pueda identificar únicamente)
 * @property {string} titulo - título cabecera del grupo, se usará el idGrupo en caso de no especificarse
 * @property {string[]} checklist - listado de textos mostrados como checklist en el encabezado del grupo
 * @property {DocumentoSolicitadoTypedef[]} documentos - listado de documentos solicitados incluyendo opcionales y requeridos
 */


/**
 * Porcion de archivo dada la necesidad de parcializar los documentos en segmentos
 * @typedef {Object} S3ChunkTypedef
 * @property {number} segmentNumber
 * @property {number} totalSegments
 * @property {number} size
 * @property {number} totalSize
 * @property {string} filename
 */

/**
 * Datos del documento cargado a S3
 * @typedef {Object} S3UploadedDocumentTypedef
 * @property {string} name - filename
 * @property {string} url -  en S3
 * @property {string} urlOrig -  en caso de haber sido cargado desde otra url
 * @property {string} contentType - content type
 * @property {tamano} size - bytes
 */

/**
 * @typedef {object} _BlobFile
 * @property {string} name - nombre del archivo
 * @property {string} url - url en caso de haber sido descargado por url
 */

/**
 * @typedef {Blob | _BlobFile} BlobFile
 */
