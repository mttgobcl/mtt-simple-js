'use strict'

const $ = require('jquery')

/** 
 * Preparar campo para usarlo como campo estado ocultandolo de la vista.
 * El elemento en SIMPLE seguirá siendo un componente textbox. 
 * @param {string} nombreCampo - nombre asignado al campo en SIMPLE
 * @returns {JQuery<HTMLElement>}
 */
function usarInputComoHidden(nombreCampo) {
  const $t = $(`input[name=${nombreCampo}]`)
  $t.attr('type', 'hidden')
  const $w = $t.closest('.campo.control-group')
  // $w.removeAttr('data-dependiente-campo')
  $w.find('.help-block, .control-label').hide()
  return $t
}


/**
 * @typedef {Object} OpcionesCambiarEstiloTextboxTypedef
 * @property {boolean} ocultarEtiqueta - ocultar etiqueta de campo
 */

/** @type  {OpcionesCambiarEstiloTextboxTypedef} */
const DEFAULTS_CAMBIAR_ESTILO_TEXTBOX = {
  ocultarEtiqueta: true
}

/**
 * Convertir elemento a estructura Material para mejorar diseño
 * @param {string} nombre - nombre del elemento
 * @param {string} iconoMaterial -  nombre del icono en Material-Icons [ fingerprint | business | account_circle | email }
 * @param {OpcionesCambiarEstiloTextboxTypedef} opciones
 */
function cambiarEstiloTextbox(nombre, iconoMaterial, opciones) {
  const opts = Object.assign({}, DEFAULTS_CAMBIAR_ESTILO_TEXTBOX, opciones)
  try {
    const $elm = $(`[name=${nombre}]`)
    const $grupo = $elm.closest('.form-group')
    const $label = $grupo.find('label, .help-block')
    const $icon = $(`<span class="input-group-addon"><i class="material-icons">${iconoMaterial}</i></span>`)
    $grupo.addClass('input-group')
    if (opts.ocultarEtiqueta) {
      $label.first().after($icon)
      $label.remove()
    } else {
      $grupo.parent().prepend($label.remove())
      $grupo.prepend($icon)
    }
    return $elm
  } catch (error) {
    console.warn(`cambioEstiloElemento ${nombre}`, error)
  }
}

module.exports = {
  usarInputComoHidden,
  cambiarEstiloTextbox
}