'use strict'

const { cambiarEstiloTextbox, usarInputComoHidden } = require('./components/textbox')
const { DropdownSimpleUI } = require('./components/select')
const { S3InputFileUploader } = require('./components/s3-input-file-uploader')
const { S3UrlFileUploader } = require('./components/s3-url-file-uploader')
const { GestorDocumentacion } = require('./components/gestor-documentacion')


module.exports = {
  cambiarEstiloTextbox,
  usarInputComoHidden,

  DropdownSimpleUI,
  GestorDocumentacionSimpleUI: GestorDocumentacion,

  S3InputFileUploader,
  S3UrlFileUploader
}