'use strict'

const core = require('./core')
const utils = require('./utils')

/**
* Datos del documento cargado a S3
* @typedef {Object} ArchivoRepositorioTypedef
* @property {string} name - filename
* @property {string} url -  en S3
* @property {string} contentType - content type
*/

/**
 * @typedef {Object} ObservacionExedocTypedef
 * @property {string} texto
 * @property {ArchivoRepositorioTypedef} archivo
 */

/**
* @typedef {Object} DatosExedocTypedef
* @property {number} idTramite
* @property {string} nombreTramite
* @property {string?} codigoCatalogacion - identificador del trámite en la catalogación de trámites RNT (usado tb por PET)
* @property {string} rutSolicitante - 12345678-K
* @property {string} nombreSolicitante
* @property {string} rutResponsable - 12345678-K
* @property {string} nombreResponsable
* @property {number?} tipoDocumentoExedoc - será inferido desde el idTipoServicio o ser especificado desde EXEDOC_CATEGORIA_TRAMITE
* @property {number?} tipoMateriaExedoc - será inferido desde el codigoCatalogacion o ser entregado usando EXEDOC_MATERIA
* @property {number} folio - Folio del servicio
* @property {string} codRegion - '01' ... '13' ... '16'
* @property {number} idTipoServicio - id registrado en RNT
* @property {string[]|string} ppus - arreglo de las ppus involugradas en el trámite
* @property {string} idTransaccion - identificador transacción, por ejemplo idExt de TGR
* @property {number} tipoComprobante - catalogación definida en EXEDOC_TIPO_COMPROBANTE
* @property {string} fechaHoraPago - formato yyyy-MM-dd 
* @property {string} sucursalPago
* @property {number} monto
* @property {ArchivoRepositorioTypedef} archivo - datos caratula resumen
* @property {ObservacionExedocTypedef[]} observaciones - listado de documentos cargados por el solicitante con su respectivo texto a ser desplegado 
*/

/**
 * @typedef {Object} ConfiguracionesExedocTypedef
 * @property {string} prefijoGrupoExedoc - prefijo usado para el grupo de destino en EXEDOC
 * @property {string} emisor 
 * @property {string} documentoAutor
 * @property {string} documentoNombreDestinatario
 * @property {string} nivelUrgencia 
 * @property {string} destinatarioUsuario
 * @property {boolean} destinatarioCopia
 */

/** @type {ConfiguracionesExedocTypedef} */
const CONFIGURACION_EXEDOC_DEFAULTS = {
  prefijoGrupoExedoc: 'RegistroAtiende',
  emisor: 'simple',
  documentoAutor: 'simple',
  documentoNombreDestinatario: "Secretario Regional Ministerial de Transporte y Telecomunicaciones",
  nivelUrgencia: 'normal',
  destinatarioUsuario: 'simple',
  destinatarioCopia: false
}



/** catalogación de trámites */
const EXEDOC_CATEGORIA_TRAMITE = {
  TRANSPORTE_PUBLICO: 91,
  TRANSPORTE_PRIVADO: 92,
  TRANSPORTE_ESCOLAR: 93
}

/**
 * En base al tipo de servicio indicado entrega la catalogación del servicio
 * @param {number} idTipoServicio
 * @returns {number} - el valor de la catalogación desde EXEDOC_CATEGORIA_TRAMITE
 */
function categoriaTramite(idTipoServicio) {
  switch (idTipoServicio) {
    case 4: //  RURAL BUS ESCOLAR 
    case 6: //  URBANO BUS ESCOLAR 
    case 8: //  RURAL MINIBUS ESCOLAR 
    case 10: //  URBANO MINIBUS ESCOLAR 
      return EXEDOC_CATEGORIA_TRAMITE.TRANSPORTE_ESCOLAR
    case 61: //  INTERURBANO TURISMO BUS/MINIBUS GENERAL 
    case 62: //  RURAL BUS/MINIBUS GENERAL 
    case 63: //  RURAL INTERURBANO BUS/MINIBUS GENERAL 
    case 64: //  RURAL INTERURBANO TURISMO BUS/MINIBUS GENERAL 
    case 65: //  RURAL TURISMO BUS/MINIBUS GENERAL 
    case 66: //  TURISMO ANFIBIO/EXPEDICION GENERAL 
    case 67: //  TURISMO BUS/MINIBUS GENERAL 
    case 68: //  TURISMO CAMIONETA 4X4 GENERAL 
    case 69: //  TURISMO JEEP 4X4 GENERAL 
    case 70: //  TURISMO LIMUSINA GENERAL 
    case 71: //  TURISMO STATIONWAGON GENERAL 
    case 72: //  URBANO BUS/MINIBUS GENERAL 
    case 73: //  URBANO INTERURBANO BUS/MINIBUS GENERAL 
    case 74: //  URBANO INTERURBANO TURISMO BUS/MINIBUS GENERAL 
    case 75: //  URBANO INTERURBANO TURISMO LIMUSINA GENERAL 
    case 76: //  URBANO RURAL BUS/MINIBUS GENERAL 
    case 77: //  URBANO RURAL LIMUSINA GENERAL 
    case 78: //  URBANO RURAL INTERURBANO BUS/MINIBUS GENERAL 
    case 79: //  URBANO RURAL INTERURBANO LIMUSINA GENERAL 
    case 80: //  URBANO RURAL INTERURBANO TURISMO BUS/MINIBUS GENERAL 
    case 81: //  URBANO RURAL INTERURBANO TURISMO LIMUSINA GENERAL 
    case 82: //  URBANO RURAL TURISMO BUS/MINIBUS GENERAL 
    case 83: //  URBANO TURISMO BUS/MINIBUS GENERAL 
    case 101: //  ESCOLAR BUS/MINIBUS ESPECIAL 
    case 121: //  URBANO RURAL TURISMO LIMUSINA GENERAL 
    case 161: //  INTERURBANO BUS/MINIBUS GENERAL 
      return EXEDOC_CATEGORIA_TRAMITE.TRANSPORTE_PRIVADO
    case 2: //  INTERURBANO BUS CORRIENTE 
    case 3: //  RURAL BUS CORRIENTE 
    case 5: //  URBANO BUS CORRIENTE 
    case 7: //  RURAL MINIBUS CORRIENTE 
    case 9: //  URBANO MINIBUS CORRIENTE 
    case 11: //  URBANO BUS EXPRESO 
    case 12: //  URBANO MINIBUS EXPRESO 
    case 13: //  URBANO BUS LOCAL 
    case 14: //  RURAL BUS PERIFÉRICO 
    case 15: //  AEROPUERTO BUS RECORRIDO FIJO 
    case 16: //  AEROPUERTO MINIBUS RECORRIDO FIJO 
    case 17: //  AEROPUERTO BUS RECORRIDO VARIABLE 
    case 18: //  AEROPUERTO MINIBUS RECORRIDO VARIABLE 
    case 19: //  URBANO AUTOMOVIL TAXI BÁSICO 
    case 20: //  INTERURBANO AUTOMOVIL TAXI COLECTIVO 
    case 21: //  RURAL AUTOMOVIL TAXI COLECTIVO 
    case 22: //  URBANO AUTOMOVIL TAXI COLECTIVO 
    case 23: //  URBANO AUTOMOVIL TAXI EJECUTIVO 
    case 24: //  URBANO AUTOMOVIL TAXI TURISMO 
    case 25: //  URBANO BUS TMV 
    case 26: //  URBANO BUS TRANSANTIAGO 
    case 181: //  AUTORIZADO TRAMITE LINEA
      return EXEDOC_CATEGORIA_TRAMITE.TRANSPORTE_PUBLICO

    default: throw new Error(`Tipo de Servicio no catalogado '${idTipoServicio}'`)
  }
}

/** materia referida del trámite en EXEDOC */
const EXEDOC_MATERIA = {
  SIN_MATERIA: 1,
  INSC_SERVICIO: 6,
  INSC_SERVICIO_BUS_MINIBUS_TROLE: 7,
  INSC_VEHICULO_BUS_MINIBUS_TROLE_REEMPLAZO: 8,
  INSC_VEHICULO_BUS_MINIBUS_TROLE_DIRECTA: 9,
  INSC_SERVICIO_BTE: 10,
  INSC_SERVICIO_TAXI_COLECTIVO: 11,
  INSC_VEHICULO_DIRECTA_20474_20867: 12,
  REINSC_VEHICULO_TRASLADO: 13,
  INSC_VEHICULO_RENUEVA_TU_TAXI: 14,
  CERT_VEHICULO_DUPLICADO_INSCRIPCION: 27,
  INSC_SERVICIOS: 36,
  INSC_VEHICULOS: 37,
  INCS_VEHICULO: 59,
}


/**
 * entregar la catalogación de materia en EXEDOC según la catalogación del trámite en RNT de SEGPRES
 * @param {string} idCatalogacion - id proceso (mismo usado como idPET para registro de acciones)
 * @returns {number} el código catalogado en EXEDOC para el trámite
 */
function materiaTramite(idCatalogacion) {
  const codigo = idCatalogacion.toString().padStart(10, '0')
  switch (codigo) {
    case '0100010029': return 2 // Inscripción de servicios de buses, minibuses y trolebuses en el RNSTP
    case '0100010042': return 3 // Inscripción de servicios de taxis colectivos en el RNSTP
    case '0100010030': return 4 // Inscripción de servicios de taxi básico, taxi turismo o taxi ejecutivo en el RNSTP
    case '0100010096': return 5 // Renovación de inscripción de servicios inscritos en el RNSTP
    case '0100010097': return 6 // Modificación de Información del responsable de un servicio inscrito en RNSTP
    case '0100010098': return 7 // Cambio de Domicilio Legal del Responsable de un Servicio inscrito en el RNSTP
    case '0100010025': return 8 // Modificación de representante(s) legal(es) en  servicios inscritos RNSTP
    case '0100010049': return 9 // Inscripción de un bus, trolebús minibús, por ingreso directo o primera inscripción, en un servicio de locomoción colectiva en el RNSTP
    case '0100010093': return 10 // Inscripción de un bus, trolebús minibús, por reemplazo o renovación por chatarrización, en un servicio inscrito en el RNSTP.
    case '0100010005': return 11 // Inscripción de un taxi por reemplazo (general o por Ley o Siniestro) o por Programa Renueva Tu Taxi en el RNSTP
    case '0100010094': return 12 // Inscripción directa de taxis por Ley 20.474/20,867 en el Registro Nacional de Servicios de Transporte de Pasajero.
    case '0100010095': return 13 // Reinscripción de vehículos en un servicio inscrito en el RNSTP, que provienen de un traslado de región o de un traslado de servicio en la misma región.
    case '0100010048': return 14 // Cambio de propietario (transferencia) en vehículos de servicios inscritos en RNSTP
    case '0100010067': return 15 // Incorporación, Eliminación o modificación de conductor inscrito en servicios en el RNSTP.
    case '0100010056': return 16 // Cancelación por traslado de región de un vehículo inscrito en un servicio en el RNSTP
    case '0100010009': return 17 // Cancelación por traslado de servicio dentro de la misma región de un vehículo inscrito en un servicio en el RNSTP.
    case '0100010012': return 18 // Cambio modalidad de un taxi inscrito en un servicio en el RNSTP
    case '0100010003': return 19 // Otras cancelaciones temporales de un vehículo inscrito en un servicio en el RNSTP  (voluntaria, por normas regionales, etc.)
    case '0100010099': return 20 // Cancelaciones definitivas (por reemplazo, antigüedad, etc.) de un vehículo inscrito en un servicio en el RNSTP.
    case '0100010050': return 21 // Cancelación de la inscripción de un servicio inscrito en el RNSTP
    case '0100010007': return 22 // Duplicado de certificado inscripción  y/o Logo (taxis RM) de un vehículo de un servicio inscrito  en el RNSTP    
    case '0100010045': return 23 // Certificación del estado actual de un vehículo o servicio en el RNSTP
    case '0100010044': return 24 // Certificado para obtener revisión técnica de un vehículo inscrito en un servicio del RNSTP o de vehículo entrante en el proceso de reemplazo en la renovación de taxis.
    case '0100010135': return 25 // Listado de Flota de servicios inscritos en el RNSTP
    case '0100010027': return 26 // Cambio o modificación de recorridos, trazados o itinerario con o sin cambio de terminal, en servicios de locomoción colectiva inscritos en el RNSTP.
    case '0100010026': return 27 // Cambio o modificación de horarios de atención por días de semana y/o de las frecuencias en los servicios de locomoción colectiva, inscritos en el RNSTP
    case '0100010043': return 28 // Modificación ubicación oficinas de ventas de pasajes en servicios interurbanos de locomoción colectiva inscritos en el RNSTP
    case '0100010100': return 29 // Informa modificación de tarifas servicios inscritos en el RNSTP
    case '0100010016': return 30 // Inscripción de Servicios en el RENASTRE
    case '0100010137': return 31 // Renovación de inscripción de servicios inscritos en RENASTRE
    case '0100010136': return 32 // Modificación de Información del responsable de un servicio inscrito en el RENASTRE
    case '0100010109': return 34 // Modificación de representante(s) legal(es) en servicios inscritos en el RENASTRE
    case '0100010102': return 35 // Inscripción de vehículos en servicios inscritos en el RENASTRE
    case '0100010117': return 36 // Cambio de propietario (transferencia) en vehículos de servicios inscritos en el RENASTRE
    case '0100010119': return 37 // Cambio en la Capacidad de Asientos en un Vehículo inscrito en el RENASTRE
    case '0100010127': return 38 // Incorporación o Eliminación de conductor inscrito en RENASTRE.
    case '0100010068': return 39 // Incorporación/Eliminación de adulto acompañante en RENASTRE
    case '0100010123': return 40 // Cancelación por traslado de región de un vehículo inscrito en el RENASTRE
    case '0100010105': return 42 // Otras cancelaciones temporales de un vehículo inscrito en el RENASTRE (voluntaria, por normas regionales, etc.)
    case '0100010120': return 44 // Cancelación de la inscripción de un servicio inscrito en el RENASTRE
    case '0100010107': return 45 // Duplicado de certificado inscripción de un vehículo de un servicio inscrito en el RENASTRE.    
    case '0100010113': return 46 // Certificación del estado actual de un vehículo o servicio en el RENASTRE
    case '0100010111': return 47 // Certificado para obtener revisión técnica de un vehículo inscrito o a inscribir en un servicio en el RENASTRE.
    case '0100010134': return 48 // Listado de Flota de servicios inscritos en el RENASTRE
    case '0100010023': return 49 // Inscripción de Servicios de TTEPRIV (Autorización General).
    case '0100010122': return 50 // Inscripción de Servicios de TTEPRIV con vehículos inscritos en RENASTRE (Autorización Especial)
    case '0100010126': return 52 // Modificación de Información del responsable de un servicio inscrito como TTEPRIV
    case '0100010130': return 53 // Cambio de Domicilio Legal del Responsable de un Servicio inscrito como TTEPRIV
    case '0100010110': return 54 // Modificación de representante(s) legal(es) en servicios inscritos como TTEPRIV
    case '0100010125': return 55 // Inscripción de vehículo en un servicio inscrito como TTEPRIV
    case '0100010118': return 56 // Cambio de propietario (transferencia) en vehículos de servicios inscritos como TTEPRIV
    case '0100010131': return 57 // Modificación a la Capacidad de Asientos en un Vehículo Que Presta Servicio de TTEPRIV (Autorización General).
    case '0100010128': return 58 // Incorporación o Eliminación de conductor en servicios inscritos como TTEPRIV
    case '0100010124': return 59 // Cancelación por traslado de región de un vehículo inscrito como TTEPRIV (Autorización General)
    case '0100010138': return 60 // Cancelación por traslado de servicio dentro de la misma región de un vehículo inscrito como TTEPRIV  (Autorización General)
    case '0100010106': return 61 // Otras cancelaciones temporales de un vehículo inscrito como TTEPRIV  (Autorización General) (voluntaria, por normas regionales, etc.)
    case '0100010139': return 62 // Cancelaciones definitivas de un vehículo inscrito como TTEPRIV  (Autorización General)
    case '0100010140': return 63 // Cancelación de un vehículo inscrito en TTEPRIV (Autorización Especial)
    case '0100010121': return 64 // Cancelación de la inscripción de un servicio inscrito en TTEPRIV
    case '0100010108': return 65 // Duplicado de constancia de autorización de un vehículo de un servicio inscrito como TTEPRIV.
    case '0100010114': return 66 // Certificación del estado actual de un vehículo o servicio en TTEPRIV
    case '0100010112': return 67 // Certificado para obtener revisión técnica de un vehículo inscrito o por inscribir en TTEPRIV (Autorización General)
    default:
      console.warn(`no se identifica catalogación para proceso '${codigo}' (${idCatalogacion}) `)
      return 0
  }
}



/** tipo de comprobante del pago catalogado  */
const EXEDOC_TIPO_COMPROBANTE = {
  DEPOSITO_EFECTIVO: 1,
  DEPOSITO_DOCUMENTOS: 2,
  TRANSFERENCIA: 3,
  BOTON_DE_PAGO: 4
}

/**
 * Entrega la codificación de Tipo de Servicio asociado al trámite electrónico usando el id de Tipo de Servicio registrado en RNT
 * @param {number} idTipoServicio - Id del tipo servicio en RNT
 * @returns {number} - código de servicio registrado en EXEDOC
 */
function tipoServicioExedoc(idTipoServicio) {
  switch (idTipoServicio) {
    case 1: return 6 //  INTERNACIONAL BUS CORRIENTE 
    case 2: return 9 //  INTERURBANO BUS CORRIENTE 
    case 3: return 13 //  RURAL BUS CORRIENTE 
    case 4: return 14 //  RURAL BUS ESCOLAR 
    case 5: return 32 //  URBANO BUS CORRIENTE 
    case 6: return 33 //  URBANO BUS ESCOLAR 
    case 7: return 17 //  RURAL MINIBUS CORRIENTE 
    case 8: return 18 //  RURAL MINIBUS ESCOLAR 
    case 9: return 39 //  URBANO MINIBUS CORRIENTE 
    case 10: return 40 //  URBANO MINIBUS ESCOLAR 
    case 11: return 34 //  URBANO BUS EXPRESO 
    case 12: return 41 //  URBANO MINIBUS EXPRESO 
    case 13: return 35 //  URBANO BUS LOCAL 
    case 14: return 15 //  RURAL BUS PERIFÉRICO 
    case 15: return 1 //  AEROPUERTO BUS RECORRIDO FIJO 
    case 16: return 3 //  AEROPUERTO MINIBUS RECORRIDO FIJO 
    case 17: return 2 //  AEROPUERTO BUS RECORRIDO VARIABLE 
    case 18: return 4 //  AEROPUERTO MINIBUS RECORRIDO VARIABLE 
    case 19: return 28 //  URBANO AUTOMOVIL TAXI BÁSICO 
    case 20: return 8 //  INTERURBANO AUTOMOVIL TAXI COLECTIVO 
    case 21: return 12 //  RURAL AUTOMOVIL TAXI COLECTIVO 
    case 22: return 29 //  URBANO AUTOMOVIL TAXI COLECTIVO 
    case 23: return 30 //  URBANO AUTOMOVIL TAXI EJECUTIVO 
    case 24: return 31 //  URBANO AUTOMOVIL TAXI TURISMO 
    case 25: return 36 //  URBANO BUS TMV 
    case 26: return 37 //  URBANO BUS TRANSANTIAGO 
    case 61: return 11 //  INTERURBANO TURISMO BUS/MINIBUS GENERAL 
    case 62: return 16 //  RURAL BUS/MINIBUS GENERAL 
    case 63: return 19 //  RURAL INTERURBANO BUS/MINIBUS GENERAL 
    case 64: return 20 //  RURAL INTERURBANO TURISMO BUS/MINIBUS GENERAL 
    case 65: return 21 //  RURAL TURISMO BUS/MINIBUS GENERAL 
    case 66: return 22 //  TURISMO ANFIBIO/EXPEDICION GENERAL 
    case 67: return 23 //  TURISMO BUS/MINIBUS GENERAL 
    case 68: return 24 //  TURISMO CAMIONETA 4X4 GENERAL 
    case 69: return 25 //  TURISMO JEEP 4X4 GENERAL 
    case 70: return 26 //  TURISMO LIMUSINA GENERAL 
    case 71: return 27 //  TURISMO STATIONWAGON GENERAL 
    case 72: return 38 //  URBANO BUS/MINIBUS GENERAL 
    case 73: return 42 //  URBANO INTERURBANO BUS/MINIBUS GENERAL 
    case 74: return 43 //  URBANO INTERURBANO TURISMO BUS/MINIBUS GENERAL 
    case 75: return 44 //  URBANO INTERURBANO TURISMO LIMUSINA GENERAL 
    case 76: return 45 //  URBANO RURAL BUS/MINIBUS GENERAL 
    case 77: return 46 //  URBANO RURAL LIMUSINA GENERAL 
    case 78: return 47 //  URBANO RURAL INTERURBANO BUS/MINIBUS GENERAL 
    case 79: return 48 //  URBANO RURAL INTERURBANO LIMUSINA GENERAL 
    case 80: return 49 //  URBANO RURAL INTERURBANO TURISMO BUS/MINIBUS GENERAL 
    case 81: return 50 //  URBANO RURAL INTERURBANO TURISMO LIMUSINA GENERAL 
    case 82: return 51 //  URBANO RURAL TURISMO BUS/MINIBUS GENERAL 
    case 83: return 53 //  URBANO TURISMO BUS/MINIBUS GENERAL 
    case 101: return 5 //  ESCOLAR BUS/MINIBUS ESPECIAL 
    case 121: return 52 //  URBANO RURAL TURISMO LIMUSINA GENERAL 
    case 141: return 7 //  INTERNACIONAL CAMION/VEH.APOYO CORRIENTE 
    case 161: return 10 //  INTERURBANO BUS/MINIBUS GENERAL 

    default:
      console.warn(`no se registra un código de servicio asociado al tipo de servicio ${idTipoServicio}`)
      return 0
  }
}

/**
 * Convierte el listado de observaciones al formato json de EXEDOC
 * @param {ObservacionExedocTypedef[]} obs 
 */
function mapObservaciones(obs) {
  return obs ? obs.map(o => ({
    texto: o.texto,
    archivo: o.archivo ? {
      nombreArchivo: o.archivo.name,
      contentType: o.archivo.contentType,
      data: o.archivo.url
    } : null,
    adjuntadoPor: "simple"
  })) : []
}


/**
 * @param {DatosExedocTypedef} datos 
 * @param {ConfiguracionesExedocTypedef} opciones
 */
function crearObjetoExedoc(datos, opciones) {
  try {
    const fechaActual = new Date()
    var objExe = {
      "emisor": opciones.emisor,
      "idSimple": datos.idTramite,
      "identificadorServicio": (datos.folio || "s/n").toString(),
      "idRegion": datos.codRegion,
      "idTipoServicio": tipoServicioExedoc(datos.idTipoServicio),
      "ppu": Array.isArray(datos.ppus) ? datos.ppus.join(';') : datos.ppus,
      "rutResponsable": datos.rutResponsable,
      "rutSolicitante": datos.rutSolicitante,
      "nombreSolicitante": datos.nombreSolicitante,
      "idTransaccion": datos.idTransaccion,
      "monto": datos.monto || 0,
      "tipoComprobante": datos.tipoComprobante || 0,
      "fechaHoraPago": datos.fechaHoraPago || '',
      "sucursalPago": datos.sucursalPago || '',
      "urlCallbackSimple": `${window.location.protocol}//${window.location.host}/backend/api/notificar/${datos.idTramite}`,
      "documento": {
        "numero": "s/n",
        "fecha": utils.toStrDatetime(fechaActual),
        "autor": opciones.documentoAutor,
        "tipoDocumento": datos.tipoDocumentoExedoc || categoriaTramite(datos.idTipoServicio),
        "tipoMateria": datos.tipoMateriaExedoc || materiaTramite(datos.codigoCatalogacion),
        "reservado": false,
        "antecedentes": "s/a",
        "materia": datos.nombreTramite,
        "nivelUrgencia": datos.nivelUrgencia,
        "emisor": datos.nombreResponsable,
        "destinatario": [opciones.documentoNombreDestinatario],
        "dataArchivo": datos.archivo.url,
        "nombreArchivo": datos.archivo.name,
        "contentType": datos.archivo.contentType
      },
      "destinatario": [{ "usuario": opciones.destinatarioUsuario, "copia": opciones.destinatarioCopia.toString() }],
      "destinatarioGrupo": [
        `${opciones.prefijoGrupoExedoc}${parseInt(datos.codRegion, 10)}`
      ],
      "observacion": mapObservaciones(datos.observaciones)
    }

    return objExe
  } catch (error) {
    return { error: "Error al crear json exedoc: " + error.message, stacktrace: error.stackTrace }
  }
}


/** 
 * Uso del campo indicado como destino del objeto deshidratado para enviar a EXEDOC 
 * @param {string} nombreCampo - nombre del campo en formulario simple
 * @param {DatosExedocTypedef} datos 
 * @param {ConfiguracionesExedocTypedef} opciones
 */
function definirCampoJsonExedoc(nombreCampo, datos, opciones) {
  const opts = Object.assign({}, CONFIGURACION_EXEDOC_DEFAULTS, opciones || {})
  const $t = core.usarInputComoHidden(nombreCampo)
  const objExedoc = crearObjetoExedoc(datos, opts)
  console.debug('definirCampoJsonExedoc', objExedoc)
  $t.val(utils.deshidratar(objExedoc))
}



module.exports = {
  EXEDOC_CATEGORIA_TRAMITE,
  EXEDOC_MATERIA,
  EXEDOC_TIPO_COMPROBANTE,

  definirCampoJsonExedoc,
  tipoServicioExedoc,
  categoriaTramite,
  materiaTramite
}
