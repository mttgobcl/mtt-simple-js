import * as helpers from "../src/js/utils";

// https://www.chaijs.com/guide/styles
describe('utils.js', function () {
  describe('obtenerTokenCSRF()', function () {
    it('vacío si no encontrado', function () {
      const res = helpers.obtenerTokenCSRF()
      assert.equal(res, '', 'entregar vacio si no es encontrado')
    });
  });
});