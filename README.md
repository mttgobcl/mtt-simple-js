# Biblioteca de recursos Simple Digital del MTT

![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/mttgobcl/mtt-simple-js)
![Open issues](https://img.shields.io/bitbucket/issues/mttgobcl/mtt-simple-js)
![npm](https://img.shields.io/npm/v/mtt-simple-js)

## Ejemplos

Para ver los ejmplos instalar dependencias e iniciar el modo _dev_ para iniciar un servidor local según configuración de [webpack.config.js](./webpack.config.js)

```shell
npm i
npm run dev
```

## npm

Para desarrollo local e instalación sin publicación en npmjs usar

```bash
npm pack
# generará archivo de nombre mtt-simple-js-{version}.tgz

# en proyecto destino
npm i {ruta_relativa}/mtt-simple-js-{version}.tgz
```
