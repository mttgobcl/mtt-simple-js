const path = require('path')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
  entry: {
    'mtt.simple.min': './index.js',
    'mtt.simple': './index.js'
  },
  devServer: {
    publicPath: '/dist',
    contentBase: './',
    watchContentBase: false,
    open: true,
    openPage: './ejemplos/01/index.html'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    publicPath: './src/assets/',
    library: 'SimpleMTT',
    crossOriginLoading: 'anonymous'
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
      include: /\.min\.js$/,
      extractComments: true,
      terserOptions: {
        compress: {
          pure_funcs: ['console.log', 'console.debug']
        }
      }
    })]
  },
  externals: {
    jquery: 'jQuery'
  },
  module: {
    rules: [
      {
        test: /\.specs\.js$/i,
        exclude: '/node_modules/',
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },
      {
        test: /\.scss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  }
};